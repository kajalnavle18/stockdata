
var sql =require('../db/dbconnection');
const request = require('request');

exports.fetechDetails=(data,result)=>{
    let date =new Date();
    if(data.stock == "" || data.stock == undefined){
        result("Stock is undefined",false);
        return;
    }
request('https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=IBM&interval=5min&apikey=demo', function (error, response, body) {
  console.error('error:', error); 
  console.log('statusCode:', response && response.statusCode); 
 
  if(error){
      result(error,false);
      return;
  }else{
         var dd = date.getDate() - 1;
        var mm = date.getMonth()+1;
        var yyyy = date.getFullYear();
        if(dd<10) dd='0'+dd;
        if(mm<10) mm='0'+mm;
    let fromateddate = yyyy+'-'+ mm+'-'+ dd;
    let CurrentTime =date.getHours();
    let time = data.stock;
    const contentData =JSON.parse(body);
    let finData = `${fromateddate} ${CurrentTime-time}:00:00`;
    console.log('body:', finData,CurrentTime-time);
    if(contentData['Time Series (5min)'] == undefined){
        result(null,false);
        return;
    }
      result(null,{"stockData":contentData['Time Series (5min)'][`${finData}`],"Time":`${finData}`})
  }
});
}