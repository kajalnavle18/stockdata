
const express = require("express");
const bodyParser = require("body-parser");
const app = express();
var sql =require('./db/dbconnection');
// parse requests of content-type - application/json
// app.use(bodyParser.json());
app.use(bodyParser.json({limit: '50mb', extended: true}));
app.use(bodyParser.json({type: 'application/json'}));

// parse requests of content-type - application/x-www-form-urlencoded
// app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
const Routing = require("./route/route.js");

Routing(app);
const WebSocket = require('ws')
let socket = new WebSocket("wss://javascript.info/article/websocket/demo/hello")

socket.onopen = function(e) {
    console.log("open")
    socket.send("My name is John");
  };
  
  socket.onmessage = function(event) {
      const json={
        trans_id: 135025,
        fill_qty: 200799, // amount of btc traded
        fill_price: 751000, // price
        fill_flags: 1,
        inbound_order_filled: true,
        currencyPair: 'BTC-INR',
        lastModifiedDate: 1591852398211 // UTC timestamp
      }
    sql.query(`INSERT INTO d1(trans_id,fill_qty,fill_price,fill_flags,inbound_order_filled,currencyPair,lastModifiedDate) VALUES(?,?,?,?,?,?,?)`,
    [json.trans_id,json.fill_qty,json.fill_price,json.fill_flags,json.inbound_order_filled,json.currencyPair,json.lastModifiedDate],(err,data)=>{
        if(err){
            throw err;
        }else{
            console.log("data insert successfully")
        }
    })

  };
  
  socket.onclose = function(event) {
    if (event.wasClean) {
      console.log(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
    } else {
      // e.g. server process killed or network down
      // event.code is usually 1006 in this case
      console.log('[close] Connection died');
    }
  };
  
  socket.onerror = function(error) {
    console.log(`[error] ${error.message}`);
  };
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});
